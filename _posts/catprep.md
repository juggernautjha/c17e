title: Belling the CAT, et al.  
url: catprep.html
date: 2025-01-11
author: Rahul Jha
tags: cat, timepass
summary: This will hopefully never be public. 
noindex: true

## Neccessary Context
Trust me, I have a much nicer blog in the drafts, I am just too lazy to publish it. Last year, I took the CAT because I wanted to hedge against a bad placements season and also because I need academic validation at regular intervals. I managed to do not poorly, and as a consequence I now have calls from Ahmedabad (shoutout my boy Arjun Kapoor) and Calcutta (hail Britannia?). Now, even though I do not plan to join any IIM (consulting eww, opportunity cost eww, lack of Math gigaeww), I want to _convert_ all my calls so that I get to dunk on consultants (personal reasons, do not ask) for the rest of my life. This (long) post is intended to be repository of questions asked in IIM interviews that I painstakingly stole from Quora. I could have used a [Google Doc](https://docs.google.com/document/d/1CH12F73Jb8crHSr3MCV2CDdroF80XjMdwfGTPRPwzhA/edit?usp=sharing), but I am a markdown enjoyer. Please bear with me if the answers sound lofty. 


## Background Questions
### Introduction
I was born in Patna, but I was brought up in Delhi. I was fortunate enough to have parents who prioritized education and allowed me to do whatever I wanted. I have always been interested in maths, particularly, and problem-solving, in general. I have been building software since I was in 6th grade. I took part in the Linguistics Olympiad and in the inaugural APLO. I then got into IIT Kanpur to study Math, where, along with academics, I explored journalism and robotics, among other things, before settling on programming. I am currently in my final semester.

### Internship
I worked with the Model Risk team at Goldman Sachs. It is one of the larger teams in the Risk division, and is tasked with validating and benchmarking all quantitative models GS uses for its activities. I worked on Commodity pricing, where I implemented a pricing scheme for european options. I was fortunate enough to get a return offer. 

### Why MBA
I like solving problems, and enjoy learning annything that helps me get better at it. 4 years of Mathematics have equipped me with an appreciation for rigour and innovative ways of approaching quantitative problems. An MBA will equip me with the tools to solve a different set of problems, and I feel it will complement my undergrad nicely. 

### You have a job offer, why do you want to do an MBA?
I like keeping my options open, and while I really enjoyed my internship, I’m not sure I want to stick with Quantitative Finance long-term. An MBA is a great way to unlock more opportunities. It gives me the flexibility to dive into tech if that’s what I decide, but without an MBA, shifting to a non-tech role might be a lot tougher.  (mumble something about opportunity cost here).


## Hobbies
I have mentioned Linguistics and Blogging as my hobbies in IIMCs application form. I know I am a retard, now I need to have a 5-minute speech on Linguistics ready. I can probably bullshit about my blog if they ask me to. 

### Linguistics
I got into Linguistics by accident. In 2019, I came across an article about the International Linguistics Olympiad and decided to take part on a whim. I got to the orientation cum selection stage, but there were better people. I learnt a lot about languages, and that 15-day camp sparked a life long appreciation for linguistics. Now I look at a lot of socio political issues from a linguistics lens. 

Will need some examples here. Maybe the Hindi Urdu debate, actively interfering with the natural course of language for political profit like de-sanskritization of Tamil. how a lot of political movements are rooted in some weird interpretation of linguistics. 

I also like to learn languages to get a better perspective on some issues, and to appreciate the differences between them. 

#### Music
I am not a pop culture buff, even though I listen to a lot of music.
I love listening to music because it’s such a great way to experience and observe different cultures. I try to explore a variety of genres from different regions—Qawwalis, indie bands, old and new Punjabi songs, and even songs in languages I don’t understand. It’s always fun trying to map the emotions or stories in the music to their meanings, even when the words are unfamiliar.

### Books
I’m an avid reader, and ratings or reviews don’t really influence my choices—I prefer discovering books on my own. When I sit down with a book, I almost always finish it in one go, so I can get through a lot of books when I’m in the mood for reading. Fiction is my favorite genre because it feels like a window into different countries and cultures. Even if the portrayal isn’t entirely accurate, it’s still an engaging way to explore new perspectives and experiences.

### TurboAutism
**Pray it never comes to this, they WILL be creeped out**.
I like to think of myself as a competitive Wikipedia consumer—I go through a lot of Wikipedia pages daily. I’m also a pretty keen observer of social networks, though that’s really just a fancy way of saying I spend way too much time on Instagram when I don’t have pressing things to do. I enjoy inferring connections between people by observing their posts over a long time, and I’ve even written scripts to automate parts of that process. It’s a fun hobby, and it’s also shaped my long-term research goals in some exciting ways.

#### Compression/Search
Can probably spitball. 

## SOP
I only applied to IIMs ABCI, and only B was brave enough to ask for an SOP. The prompt is:
    Prepare a short essay of 600 words on yourself in the space provided below. You may wish to talk about your
background, significant events, accomplishments, experience at your workplace, extracurricular activities,
relationships with friends and family, career plans and how the Post Graduate Programme in Management from
IIMB fits into your dreams and ambitions. Please make sure that your essay forms a coherent whole.

Here is my attempt. 

I like problems. And I want to learn tools to tackle them.
As a kid, this meant spending hours solving math problems. When I got access to a computer in 6th grade, it opened the door to an entirely new family of challenges. I remember teaching myself Visual Basic (which no one uses today, fortunately) and spending weeks trying to write a web browser. Programming felt like a superpower, allowing me to tackle problems I didn't yet have the mathematical maturity to approach.
Toward the end of 10th grade, by pure happenstance, I stumbled into the fascinating world of linguistics puzzles. Languages are intricate systems, and solving these puzzles required me to master a new set of tools. It was exciting, and I gained a new way of looking at things. 
Eventually, I took the JEE and landed at IIT Kanpur. I chose Mathematics because I believed it would equip me with tools to solve even more exciting problems. While I did take my fair share of Math/CS courses, I was blissfully unaware of the kinds of puzzles the "real" world had to offer.
Parallel to academics, I pursued my passion for writing, which had always been a quiet companion. I have always had a blog. I joined IITK's journalism body to explore it further. This decision turned out to be transformative. Through my work there, I interacted with a diverse array of people from all corners of campus life. I interviewed professors about their research, listened to the stories of mess workers, and even wrote (terrible) jokes for the Freshers' issue. These experiences opened my mind to problems I had never considered before—problems that didn't have clear or "correct" solutions and problems that affected people in tangible, material ways.
This newfound perspective was a wake-up call. Through my stint in the journalism body and my experience leading the Programming Club in my third year, I realized how inadequate my existing toolkit was when faced with real-world challenges. Mathematical objects are well-behaved, and computers—apart from the occasional shenanigans—are predictable and easy to talk to. Real people, however, require a completely different set of skills I lacked. I embraced this discomfort, seeing it as just another problem to tackle. I led a team of 10 students to a bronze place finish at the Inter IIT; the technical problem we worked on was exciting, but leading a team was a lot more fulfilling than seeing numbers on the screen go big. There were disagreements and mistakes, but we were proud of what we had built. That experience was the first time I thought of seriously pursuing management education. My internship at an Investment Bank furthered my resolve. I was working on a Mathematical model for pricing derivatives, and while the math was stimulating, I saw how decision-making demanded much more than just analytical skills. It required collaborating across teams and appreciating diverse perspectives. This realization cemented my interest in pursuing management education. I believe a rigorous management education will complement my existing skills, and what better place than IIMB - with its rigorous curriculum and location - to learn management. The case-based pedagogy is exciting; reading about a problem and discussing all the different ways of solving it with an excellent peer group will equip me with a new set of tools that will be relevant in the real world. It also helps that 3 Idiots was shot here. As far as my career plans after PGP are concerned, in the short term, I want to end up in an overtly impactful and fast-paced role that values problem-solving and where my
background will be valued. In the long term, I want to leave an indelible (hopefully positive) impact in whatever industry I work in. A slightly selfish objective is to end up in books.







