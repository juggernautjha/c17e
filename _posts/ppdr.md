title: Pretentious Pakistani Drama Reviews
url: pprd.html
date: 2021-02-14
author: Rahul Jha
tags: pprd, review
summary: My transition to the dark side
noindex: true

## An insincere explanation 🤷
Yes, I did it. I have started watching dramas (not daily soaps, mind you) from the wrong side of the LOC. My relationship with Pakistani dramas began like all morganatic relationships do, in a moment of weekness. After IIT KGP has released the results, I had exactly 30 days to explore shit before I started at an IIT. I had run out of novels because in the days between the JEE and and the results, I devoured everything Agatha Christie had to offer. I was bored, and distraught, when Momina Duraid gave me her shoulder to lean on. And it was with some hesitation that I started watching the contraband,
Why Pakistani dramas, you ask? Surely there are dramas from the wrong Korea, Turkey and from an assorted bunch of nations. The onlu problem is that they do not speak my tongue. Call me crazy, but I am staunchly anti-subtitles and I therefore restrict myself to entertainment from English speaking countries and the subcontinent. I have had enough of the Queen's realm from the multiple Christie novels (they are mostly set in rural England), and the Indian television industry is a crime against the human race right up there with calling water-polo a sport. That leaves Pakistani Dramas. So they had the perfect product market fit from day one in my case.

## Introducing PPRD
Now, I consider myself to be a productive kid. I won't just watch dramas, I'll critque them. Also because I am very patriortic if you ignore my stand on the Kashmir issue, I cannot let reviews of Pakistani dramas occupy the prime real estate on my home page. All my pretentious reviews will be dumped under the tag pprd (click the link at the very top of this post, just below the title. Come on, it can't be too difficult). First up is a review of Chupke Chupke, a romcom that boasts a relatively unknown cast. Stay tuned. 

