url: seriouslythistime.html
date: 2024-03-29
title: On masking and bottlenecks.
author: Rahul Jha
tags: maths, life, games
summary: That is NOT a sexual reference

### Prologue

I know I have been incommunicado for (checks notes) a long time, I was busy having too much sex. Honestly, I do not think I have taken a longer hiatus from writing. I have been away for so long that I accidentally deleted the local copy of this repository on my laptop and did not notice. I have been away for so long that I forgot what heading style to use for section headings. I have been away for so long that, well you get the idea. This post is not supposed to be a recap because I do not remember much (you see, I was indeed busy having a lot of sex). What follows is a bunch of incoherent thoughts held together by nothing but my newfound appreciation for trap punjabi remixes.

### On Movies

I am unreasonably proud of my piracy skills (I can barely strip DRMs, all I can do is enable VPN to torrent on college WiFi) and therefore was a staunch opponent of spending money on movie tickets. I say was because I have been converted, in part because I now have some cash to burn, and also because going to the movies invariably involves a trip to what might be the only McDonalds in Kanpurland. So when a friend asked if I would watch Dune, I said yes involuntarily before realising my memory is not what it used to be and I had forgotten most things from Dune 1. A couple hours of dilly-dallying later, I was as well versed in the politics of Arrakis as one can be. The movie was fun, easily one of the best movies I have watched in a long time, and the food was better. I do not have any humourous anecdotes because the company was 60% boring and I had no control over the composition. Recently, I dragged my sibling to watch Mazgaon Express because I wanted to checkout a new mall that had opened near my home, and I was not disappointed. I might start a film review series, but given my consistency, I do not think that is a good idea. At this point I am just rambling, which brings me to

### Blackouts

I do not remember a lot of what happened that day so this section shall remain empty. All I know is that my capacity is roughly 300 mL of alcohol, and I actually cannot sing along to Seedhe Maut even though I know the lyrics. Apparently, Ghazipur ka Khatta is not my pehchan.

### Libel

I do not think I am a hater, but I cannot stand the Gymkhana. I like to think a lot of what happens around me does not affect me directly, and I therefore ignore much of the going-ons in the campus. Are you mad Vox used pink color on their Instagram, too bad. Hall 5 decided to kick their dogs to Hall 2? I have never really liked either hall, so who cares. Hall 6 won Galaxy? How nice of them. Last december though, I was foolish enough to accept a place on a review board to review a couple of contentions against my previous employers Messrs Vox Populi and Co. and if I could go back in time to change one thing I would still go to the day I first texted her. So anyway, I joined the review board in December but the first meeting was exactly 70 days later in February. After a productive 4 hours of discussions we arrived at a conclusion that obviously did not sit well with Vox. In a democracy, disagreements have a place, and this is one of the reasons why I truly believe I will thrive in a majoritarian autocracy. They did not accept the conclusion and decided to sue the review board (disclaimer, I am using legal terms because I find Suits cool and I cannot be bothered to use layman terms), again, their right because IITK is ostensibly very democratic. I was with them each step of the way, and then they proceeded to completely destroy me in the Students' Senate in front of what seemed like half the college. My credibility was questioned, and according to them it was evidenced by the fact that I had only been in Vox for over a year and had no publishing experience (I have a feeling that I was in the top three most published assistant editors in my time, but let us ignore that for the sake of argument). Valid points, but you do not actually need to have an internship with the Sullivan's (that is a Succession reference) to do pattern matching between texts. They also claimed I had little knowledge of Gymkhana procedures, conveniently disregarding the fact that I have (with some degree of success) run an SnT club for over a year and any student unfortunate enough to find himself (the SnT is mostly male :shrug:) in the SnT core team knows Gymkhana processes slightly better than the Parlimentarian - the incumbent has all the markings of an asshole but like Ali G said, I digest - so even that point is moot. I, however, was unable to put either of these points in front of the Senate because I did not think it would affect me. It did. When people you spend practically all day with decide to bullshit about you in a public forum without warning you, it is slightly difficult to b articulate. The matter was put to vote, and they lost by a vote. I retreated to my humble abode because of a quiz the next day, missing some major drama.

### Growing Up

In other news, I will be moving to Bangalore for a grand total of nine weeks to work for a bank. I know, it is very un-laal-salaam of me but it was the only internship I was able to con my way into and I truly hope they do not force me to write frontend software. In Bangalore, I am looking forward to (in no particular order) south indian food (I ate exclusively north Indian food in Madras, I know I am a legend), disposable cash (my phone is probably counting it days but the speaker sucks so hard that I cannot hear it), and old monk tetrapaks. Also the Karnataka excise act is unclear about the legal drinking age so I guess I am getting shitfaced.

### Two Blogs In One Day?

Yes, if I decide not to sleep. Also please tell me what is the correct ettiquette to ask a girl out for an online movie date (because we pirate).
