title: Big Rant
url: rant.html
date: 2022-12-09
author: Rahul Jha
tags: scraping,rant
summary: Yup, I am trying to make the title as vague as possible.

## Abstract

What is life if not a series of royal screwups? So after royally screwing up my third semester (I do have a thing against odd semesters, and yes, extrapolating from next to no data is the way to go), I have decided to mend my ways. But all that can wait till the fourth semester. I like even semesters. The purpose of this blog post, other than wasting time on a train journey, is to test my laptop's keyboard and touch typing skillz. Spoiler alert, my skillz suck. Why do you think I am single? Also, pardon me, the rant becomes sad; I was listening to the masterpiece that is Biba.

## Vulnerabilities

Long ago, I worked on an article on Data Security for Vox Populi. That they are probably about to kick me out next semester because of my terrible ideation, Vox Populi is a fun place to work in. I got to meet people who, under ordinary circumstances, wouldn't even acknowledge my existence because I had to use line feeds to make my resume a page long. When we spoke to a source regarding the vulnerabilites in the campus network, he mentioned a flaw (he won't tell us where, because telling a bunch of 18 year olds where to get everyone's phone number isn't neccessarily the best idea) which made home addresses, phone numbers, and some other data public. One day (night), while working on my web scraping skillz (I was copying requests and sending them from Postman. Much skillz.) I found the flaw. I then emailed the relevant professors because I fancy myself as a law-abiding citizen of the Republic of Kalyanpur. They seen zoned me. Or maybe they had disabled read receipts (much like a couple of monsters I am friends with). Either way, I was hurt (and you thought years of being seen zoned would've  tempered me). Anyway, I am now the proud owner of Student Search Pro Version. It is like Student search, but it also indexes everyone's address and phone number along with more metadata. My friends have access, of course, and this is going on my resume. Drop me a Hi if you want to talk about it or if your first name ends in a vowel. My DMs runneth dry.

## FFMpeg and Piracy

Remember HelloIITK? More precisely, remember the era when lecture videos were uploaded to HelloIITK? If I remember correctly (I do), the videos were not contiguous but instead served as playlists. A long video was broken up into small chunks, each chunk no more than a few seconds long. The CDN then served the chunks to the browser, which pieced them together. While I do not know why the good folks at Mookit would do such a thing (trust me, no one wants to sit through the proof of Bolzano-Weierstrass Theorem), it does make a lot of sense for video streaming sites. Video streaming sites (especially the shady ones) thrive on advertising revenue. Everytime you click on the popup that says an unhealthy number of middle aged women in Kalyanpur suddenly want to talk to you (I am making this up, trust me), someone in Cyprus makes a couple of cents (this information should be enough for you to figure out the illicit site I frequent the most). Therefore, they want to click on more popups, and if you just download videos, it would be a loss of their livelihood. Of course I had to do it. 68 lines of very-inelegant python code and FFMpeg spaghetti (if you do not what FFMpeg is, you're probably on windows and I don't like you) later, I became the very proud owner of a web-app I call mouse. It takes in just one input, the URL pattern of the site. It is easy to figure out. And then, it acts as an (ad-free) skin on top of it, allowing the user to stream and/or download videos. I cannot share the source code publically because my version is tailored to my needs, and I do not want y'all crowding my shady site. If you are a friend, DM me for the repository (however, if you are a friend, I would have already shared the file with you).

## Text2Speech

Who doesn't like audiobooks? I don't; the question wasn't rhetorical. After my performance this semester, I had the self-confidence to email professors, begging for anything for the summer. Yesterday (I kid thee not), I was going through the University of Uppsala's page when I noticed something. They are subscribed to a service that converts selected HTML to speech. Of course, I launched a 360-noscope attack on them by pulling up the console. Even after almost a year of working with frivolous websites and web services, the console excites me. Turns out, they make unprotected API calls to this service. I may not be a fan of audiobooks, I am a fan of neglected API endpoints. Within 15 minutes, I had pieced together the most grotesque utility ever written. It takes in strings and returns mp3s. This code also turns out the service UU subscribed to is very efficient. 10 chapters of a book took 10 seconds to be converted to audio. Visit my gitlab for the code.

PS: Speaking of neglected endpoints, legend has it that there is an endpoint where everyone's CPI is public. The day I find it, I'll retire from web scraping.

## Rant

Barney Stinson, of Bro-Code fame, is singlehandedly responsible for more than half of my problems. I will obviously not elaborate on a public forum (yes Karen, an audience of four people is still public) but if the bro-code didn't exist I'd still be yapping about how difficult it is to talk to people in real life. Long story short, I have nothing against Barney. I know I am making no sense, but if you expect rants to make sense, I would like to direct you to Satyadev Nandakumar's blog. The guy is an inspiration.

## Note:

I know the comments do not work because Heroku decided to stop the free/hobby plan, and I am not prepared to pay 25 bucks a month for a quarterly blog. I am in the process of migrating comments; bear with me. 

PS: FIxed it! Screw Heroku.
