import time
import datetime
import sys


'''
url: elo-1.html
date: 2022-1-31
author: Rahul Jha
tags: bad math, economics, go, ors
summary: And other hot takes
'''


file_name = "_posts/{}.md".format(sys.argv[1])
url = "{}.html".format(sys.argv[2])
now = datetime.datetime.now()
date = now.strftime("%Y-%m-%d")

print("{}\n{}\n{}".format(file_name, url, date))

with open(file_name, "w") as f:
    f.write("url: {}\n".format(url))
    f.write("date: {}\n".format(date))
    f.write("author: Rahul Jha\n")
    f.write("tags: \nsummary: \n")


